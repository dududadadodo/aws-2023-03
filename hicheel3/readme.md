# AWS хичээл 3 - Storage

Тус хичээл дээр гэрийн даалгавар давтан хийсэн ба Storage сэдвийн талаар үзсэн

- [Хичээл бичлэг 1](https://youtu.be/NpQPuz0dM_Y)
- [Хичээл бичлэг 2](https://youtu.be/uSdo8aIcNS0)


# Нэмэлт материалууд

- https://aws.amazon.com/s3/faqs/
- https://aws.amazon.com/ebs/faqs/
- https://aws.amazon.com/efs/faq/
- https://tutorialsdojo.com/amazon-s3-vs-ebs-vs-efs/
- https://tutorialsdojo.com/amazon-ebs/
- https://tutorialsdojo.com/amazon-efs/
- https://tutorialsdojo.com/amazon-s3-vs-glacier/



