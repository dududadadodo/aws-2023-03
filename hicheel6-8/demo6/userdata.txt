#!/bin/bash
apt-get update -y && apt-get install -y docker.io
apt-get install -y docker-compose
sudo usermod -aG docker $(whoami)
sudo systemctl start docker
sudo systemctl enable docker