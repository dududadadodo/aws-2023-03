# AWS хичээл 4 - VPC

Тус хичээл дээр VPC буюу Virtual private cloud-г үзсэн

- [Хичээл бичлэг 1](https://youtu.be/KEgDZa4ekuk)

# Даалгавар

-  192.168.0.0/24 сүлжээтэй VPC сүлжээ үүсгэнэ. Дотор нь 4 тусдаа subnet үүсгэнэ (2 нь private, 2 нь public). Internet gateway, NAT gateway үүсгэнэ. Routing table дээр мөн шаардлагатай тохиргоог хийнэ. Үр дүнд нь public subnet дотор үүссэн instance интернетээс хандалттай. private subnet дотор үүссэн instance интернетээс хандалтгүй боловч гадагшаа хандах боломжтой байдлаар зохион байгуулна.

Subnet 1 - 100 hosts
Subnet 2 - 60 hosts
Subnet 3 - 25 hosts
Subnet 4 - 20 hosts

 - Үүссэн серверүүдийн Snapshot болон Image үүсгэж үзнэ. Урсгал болон эдгээрийн ялгааг сайн ажиглана.
 - Үүссэн серверийг өөр **AZ**, **Region** дээр copy-дож асаана.
 - NAT gateway-г NAT instance-р сольж үзнэ.
 
 
0. Delete default VPC
1. Create a VPC
2. Create Subnets /different AZs/
Enable auto assign IPv4 for public subs
3. Create and attach Internet gateway
4. Create a public instance (Sec group - Allow ICMP - ping)
5. Create 2 route tables
6. Associate subnets on the route table
7. create Rules (0.0.0.0/0 -> Internet gateway)
8. Create a NAT gateway
9. Edit Private route table 
0.0.0.0/0 -> NAT gateway
10. Creata a private instance
11. SSH to private server through the public server


# Нэмэлт материалууд
- https://aws.amazon.com/rds/faqs/
- https://cloud.netapp.com/blog/aws-cvo-blg-aws-rds-pricing-explained
- https://tutorialsdojo.com/amazon-vpc/
- https://tutorialsdojo.com/aws-direct-connect/
- https://tutorialsdojo.com/aws-transit-gateway/
- https://tutorialsdojo.com/vpc-peering/
- https://tutorialsdojo.com/amazon-aurora-vs-amazon-rds/



# Нэмэлт материалууд

- https://aws.amazon.com/vpc/faqs/
- https://tutorialsdojo.com/amazon-vpc/
